package gb;

public final class Const {
	public final static String str_ConfTableName = "Configuration";
	public final static String str_User = "User";
	public final static String str_Password = "Password";
	public final static String str_GitRepoURL = "GitRepoURL";
	public final static String str_FileRepository = "FileRepository";
	public final static String str_CommitName = "CommitName";
	public final static String str_CommitEmail = "CommitEmail";
	public final static String str_RepoPathName = "RepoPathName";
	public final static String str_InitialBranch = "BranchName";
	//PlatformInfo property Names
	public final static String str_PlatformInfo_URLPropertyName="URL";
	public final static String str_PlatformInfo_AppKeyPropertyName = "AppKey";
	
	//Mashup names
	public final static String str_HomeMashupName="GitBackup.Home.Mashup";

}
