package gb;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jgit.api.CreateBranchCommand;
import org.eclipse.jgit.api.CreateBranchCommand.SetupUpstreamMode;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.api.ResetCommand.ResetType;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.slf4j.Logger;

import com.thingworx.data.util.InfoTableInstanceFactory;
import com.thingworx.entities.utils.EntityUtilities;
import com.thingworx.logging.LogUtilities;
import com.thingworx.metadata.annotations.ThingworxBaseTemplateDefinition;
import com.thingworx.metadata.annotations.ThingworxConfigurationTableDefinition;
import com.thingworx.metadata.annotations.ThingworxConfigurationTableDefinitions;
import com.thingworx.metadata.annotations.ThingworxDataShapeDefinition;
import com.thingworx.metadata.annotations.ThingworxFieldDefinition;
import com.thingworx.metadata.annotations.ThingworxServiceDefinition;
import com.thingworx.metadata.annotations.ThingworxServiceParameter;
import com.thingworx.metadata.annotations.ThingworxServiceResult;
import com.thingworx.relationships.RelationshipTypes.ThingworxRelationshipTypes;
import com.thingworx.resources.entities.EntityServices;
import com.thingworx.things.Thing;
import com.thingworx.things.repository.FileRepositoryThing;
import com.thingworx.types.InfoTable;
import com.thingworx.types.collections.ValueCollection;
import com.thingworx.types.primitives.BooleanPrimitive;
import com.thingworx.types.primitives.StringPrimitive;

@ThingworxBaseTemplateDefinition(name = "GenericThing")

@ThingworxConfigurationTableDefinitions(tables = {
		@ThingworxConfigurationTableDefinition(name = Const.str_ConfTableName, description = "", isMultiRow = false, ordinal = 0, dataShape = @ThingworxDataShapeDefinition(fields = {
				@ThingworxFieldDefinition(name = Const.str_User, description = "", baseType = "STRING", ordinal = 0, aspects = {}),
				@ThingworxFieldDefinition(name = Const.str_Password, description = "", baseType = "PASSWORD", ordinal = 1, aspects = {}),

				@ThingworxFieldDefinition(name = Const.str_CommitName, description = "", baseType = "STRING", ordinal = 2, aspects = {
						"friendlyName:Commit Name", "defaultValue:John Doe" }),

				@ThingworxFieldDefinition(name = Const.str_CommitEmail, description = "", baseType = "STRING", ordinal = 3, aspects = {
						"friendlyName:Commit Email", "defaultValue: jdoe@ptc.com" }),

				@ThingworxFieldDefinition(name = Const.str_GitRepoURL, description = "", baseType = "STRING", ordinal = 4, aspects = {
						"defaultValue:https://bitbucket.org/username/reponame", "friendlyName:Git Repo URL" }),

				@ThingworxFieldDefinition(name = Const.str_FileRepository, description = "", baseType = "THINGNAME", ordinal = 5, aspects = {
						"thingTemplate:FileRepository", "friendlyName:File Repository","defaultValue: GitRepository" }),

				@ThingworxFieldDefinition(name = Const.str_RepoPathName, description = "", baseType = "STRING", ordinal = 6, aspects = {
						"friendlyName:File Repository Path", "defaultValue:/smartparking" }),

				@ThingworxFieldDefinition(name = Const.str_InitialBranch, description = "", baseType = "STRING", ordinal = 7, aspects = {
						"friendlyName:Initial branch", "defaultValue:master" }),

		})) })
public class GitBackupTemplate extends Thing {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6500080561143490845L;

	// Complete git path will be calculated by concatenating the SCR absolute
	// path and the relative path
	private String str_User, str_Password, str_BitbucketRepoPath, str_FileRepository, str_CommitName, str_CommitEmail,
			str_FileRepoPath, str_CurrentBranchOrCommit;
	private boolean isDetachedHead = false;

	private static Logger _logger = LogUtilities.getInstance().getApplicationLogger(GitBackupTemplate.class);

	public GitBackupTemplate() {

	}

	@Override
	protected void initializeThing() throws Exception {
		this.str_User = ((String) getConfigurationSetting(Const.str_ConfTableName, Const.str_User));
		this.str_Password = ((String) getConfigurationSetting(Const.str_ConfTableName, Const.str_Password));
		this.str_BitbucketRepoPath = ((String) getConfigurationSetting(Const.str_ConfTableName, Const.str_GitRepoURL));
		this.str_FileRepository = ((String) getConfigurationSetting(Const.str_ConfTableName, Const.str_FileRepository));
		this.str_FileRepoPath = ((String) getConfigurationSetting(Const.str_ConfTableName, Const.str_RepoPathName));
		this.str_CommitName = ((String) getConfigurationSetting(Const.str_ConfTableName, Const.str_CommitName));
		this.str_CommitEmail = ((String) getConfigurationSetting(Const.str_ConfTableName, Const.str_CommitEmail));
		this.str_CurrentBranchOrCommit = ((String) getConfigurationSetting(Const.str_ConfTableName, Const.str_InitialBranch));
		
		super.initializeThing();
		
		Git Mygit = GetRepository();
		String str_Branch = Mygit.getRepository().getFullBranch();
		isDetachedHead =  (str_Branch.indexOf("refs/heads")!=-1||str_Branch!=null) ? false:true;
		str_CurrentBranchOrCommit = (str_Branch!=null) ? Mygit.getRepository().getBranch():"MASTER";
		
		this.SetHomeMashup(Const.str_HomeMashupName);
		if (!this.implementsShape("Git.Utility.ThingShape")) {
			EntityServices es = new EntityServices();
			es.AddShapeToThing(this.getName(), "Git.Utility.ThingShape");
		}
	}

	@ThingworxServiceDefinition(name = "Push", description = "This will execute a push of all the files for the specific project. Be warned that you might need to edit the global gitignore file to include file types you might want in the commit, like log files. This is usually stored in Windows in the the [user]/Documents/gitignore_global.txt ", category = "", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "STRING", aspects = {})
	public String Push(
			@ThingworxServiceParameter(name = "Message", description = "A message that will appear in the git for this commit", baseType = "STRING") String Message)
			throws Exception, GitAPIException {
		_logger.trace("Entering Service: Push");
		try {
			Git myGitFolder = GetRepository();
			myGitFolder.add().addFilepattern(".").call();

			myGitFolder.add().addFilepattern(".").setUpdate(true).call();

			myGitFolder.commit().setAll(true).setMessage(Message).call();

			CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(str_User, str_Password);

			myGitFolder.push().setRemote("origin").setCredentialsProvider(credentialsProvider).call();

			myGitFolder.getRepository().close();
			myGitFolder.close();
			_logger.trace("Exiting Service: Push");
			return "Push successfull!";
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			_logger.error(errors.toString());
			return "Push Error: " + errors.toString();
		}

	}

	private Git openOrCreate(File gitDirectory) throws IOException, GitAPIException {
		Git git;
		FileRepositoryBuilder repositoryBuilder = new FileRepositoryBuilder();
		repositoryBuilder.addCeilingDirectory(gitDirectory);
		repositoryBuilder.findGitDir(gitDirectory);
		if (repositoryBuilder.getGitDir() == null) {
			git = Git.init().setDirectory(gitDirectory).call();
		} else {
			git = new Git(repositoryBuilder.build());
		}
		return git;
	}

	@ThingworxServiceDefinition(name = "Pull", description = "Pulls the last commit to the File Repository path", category = "", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "STRING", aspects = {})
	public String Pull(
			@ThingworxServiceParameter(name = "Force", description = "Forces a hard reset instead of a normal pull", baseType = "BOOLEAN") Boolean Force)
			throws IOException, GitAPIException {
		_logger.trace("Entering Service: Pull");
		try {

			Git myGitFolder = GetRepository();
			CredentialsProvider credentialsProvider = new UsernamePasswordCredentialsProvider(str_User, str_Password);
			if (Force != null && Force == true) {
				myGitFolder.reset().setMode(ResetType.HARD).call();
			}
			myGitFolder.pull().setCredentialsProvider(credentialsProvider).call();
			
			myGitFolder.getRepository().close();
			myGitFolder.close();

			_logger.trace("Exiting Service: Pull");
			return "Pull successfull!";
		} catch (Exception e)

		{
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			_logger.error(errors.toString());
			return "Pull Error: " + errors.toString();

		}

	}

	@ThingworxServiceDefinition(name = "DeleteLocalRepoContent", description = "Deletes all files from the local repo path including the git configuration files. This operation is needed in case the git operations throw up strange errors.", category = "", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "NOTHING", aspects = {})
	public void DeleteLocalRepoContent() throws IllegalStateException, GitAPIException, IOException {
		_logger.trace("Entering Service: ResetLocalRepo");
		FileRepositoryThing srcRepo = (FileRepositoryThing) EntityUtilities.findEntity(str_FileRepository,
				ThingworxRelationshipTypes.Thing);
		String str_FolderPath = srcRepo.getRootPath();
		str_FolderPath += str_FileRepoPath;
		deleteDirectory(new File(str_FolderPath));

		_logger.trace("Exiting Service: ResetLocalRepo");
	}

	private boolean deleteDirectory(File path) {
		if (path.exists()) {
			File[] files = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteDirectory(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}

	// private void ExportExtensionList(String str_FolderPath) throws Exception
	// {
	// Subsystem platform = (Subsystem)
	// EntityUtilities.findEntity("PlatformSubsystem",
	// ThingworxRelationshipTypes.Subsystem);
	// InfoTable iftbl_Extensions =
	// platform.processServiceRequest("GetExtensionPackageList", null);
	// File fil = new File(str_FolderPath + "//" + "Extensions.log");
	// FileWriter fw = new FileWriter(fil);
	// BufferedWriter bw = new BufferedWriter(fw);
	// JSONObject json = iftbl_Extensions.toJSON();
	// // we remove the datashape node from the extensions JSON in order to
	// // make it more readable by the user
	// json.remove("dataShape");
	// bw.write(json.toString());
	// bw.close();
	// }

	// @ThingworxServiceDefinition(name = "ViewExtensionsList", description =
	// "Displays a list of the extension that are needed for preloading before
	// importing the project. Please note that an extension might be listed here
	// but not necessarily required for this project.", category = "",
	// isAllowOverride = false, aspects = {
	// "isAsync:false" })
	// @ThingworxServiceResult(name = "Result", description = "", baseType =
	// "INFOTABLE", aspects = {
	// "isEntityDataShape:true" })
	// public InfoTable ViewExtensionsList() throws Exception {
	// _logger.trace("Entering Service: ViewExtensionsList");
	// FileRepositoryThing srcRepo = (FileRepositoryThing)
	// EntityUtilities.findEntity(str_FileRepository,
	// ThingworxRelationshipTypes.Thing);
	// // String str_FolderPath = (String)
	// // srcRepo.getConfigurationSetting("settings", "rootPath");
	// String str_FolderPath = srcRepo.getRootPath();
	// str_FolderPath += str_FileRepoPath;
	// File fil = new File(str_FolderPath + "//" + "Extensions.log");
	// InfoTable iftbl_Result =
	// InfoTableInstanceFactory.createInfoTableFromDataShape("ExtensionPackage");
	// JSONObject json = new JSONObject(new
	// String(Files.readAllBytes(fil.toPath())));
	// JSONArray rows = ((JSONArray) json.get("rows"));
	//
	// for (int i = 0; i < rows.length(); i++) {
	// JSONObject row = rows.getJSONObject(i);
	//
	// ValueCollection vc = new ValueCollection();
	// vc.put("name", new StringPrimitive(row.getString("name")));
	// vc.put("description", new StringPrimitive(row.getString("description")));
	// vc.put("packageVersion", new
	// StringPrimitive(row.getString("packageVersion")));
	// vc.put("vendor", new StringPrimitive(row.getString("vendor")));
	//
	// vc.put("minimumThingWorxVersion", new
	// StringPrimitive(row.getString("minimumThingWorxVersion")));
	// iftbl_Result.addRow(vc);
	// }
	//
	// _logger.trace("Exiting Service: ViewExtensionsList");
	// return iftbl_Result;
	// }

	// @ThingworxServiceDefinition(name = "ExportExtensionToFileRepository",
	// description = "Works only if you are part of the Administrators group and
	// if you have the ExportExtensions extension in your system", category =
	// "", isAllowOverride = false, aspects = {
	// "isAsync:false" })
	// @ThingworxServiceResult(name = "Result", description = "", baseType =
	// "STRING", aspects = {})
	// public String ExportExtensionToFileRepository() {
	// String str_LogMessage = "Export Successfull!";
	// _logger.trace("Entering Service: ExportExtensionToFileRepository");
	// Resource exportExt = (Resource)
	// EntityUtilities.findEntity("ExportExtensionUtilities",
	// ThingworxRelationshipTypes.Resource);
	// ValueCollection vc = new ValueCollection();
	// vc.put("repositoryName", new StringPrimitive(str_FileRepository));
	// vc.put("repositoryPath", new StringPrimitive(str_FileRepoPath +
	// "/Extensions"));
	// vc.put("overwrite", new BooleanPrimitive(true));
	// try {
	// exportExt.processServiceRequest("ExportExtensionsToRepository", vc);
	// } catch (Exception e) {
	//
	// StringWriter errors = new StringWriter();
	// e.printStackTrace(new PrintWriter(errors));
	// _logger.error(errors.toString());
	// str_LogMessage = errors.toString();
	// }
	//
	// _logger.trace("Exiting Service: ExportExtensionToFileRepository");
	// return str_LogMessage;
	// }

	// @ThingworxServiceDefinition(name = "ImportExtension", description = "This
	// method imports an extension in ThingWorx from repository setup in the
	// Configuration tab.", category = "", isAllowOverride = false, aspects = {
	// "isAsync:false" })
	// @ThingworxServiceResult(name = "Result", description =
	// "alert('mymessage')", baseType = "STRING", aspects = {})
	// public JSONObject ImportExtension(
	// @ThingworxServiceParameter(name = "FileRepoPath", description = "",
	// baseType = "STRING", aspects = {
	// "defaultValue:\\Extensions\\extensionName.zip" }) String FileRepoPath)
	// throws Exception {
	// _logger.trace("Entering Service: ImportExtension");
	//
	// Thing th_PlatformInfo = (Thing)
	// EntityUtilities.findEntity("PlatformInfo",
	// ThingworxRelationshipTypes.Thing);
	// ValueCollection vc = new ValueCollection();
	// JSONObject headers = new JSONObject();
	// JSONObject responseheaders = new JSONObject();
	// headers.put("X-XSRF-TOKEN", "TWX-XSRF-TOKEN-VALUE");
	// headers.put("appKey",
	// th_PlatformInfo.GetStringPropertyValue(Const.str_PlatformInfo_AppKeyPropertyName));
	//
	// ContentLoader contentLoader = new ContentLoader();
	//
	// // Resource exportExt = (Resource)
	// EntityUtilities.findEntity("ContentLoaderFunctions",
	// // ThingworxRelationshipTypes.Resource);
	//
	//
	// //vc.put("headers", new JSONPrimitive(headers));
	// //vc.put("ignoreSSLErrors", new BooleanPrimitive(true));
	//
	// //vc.put("repository", new StringPrimitive(str_FileRepository));
	// //vc.put("url", new
	// StringPrimitive(th_PlatformInfo.GetStringPropertyValue(Const.str_PlatformInfo_URLPropertyName)+
	// "/ExtensionPackageUploader?purpose=import&validate=false"));
	// //vc.put("pathOnRepository", new
	// StringPrimitive(str_FileRepoPath+FileRepoPath));
	// //vc.put("overwrite", new BooleanPrimitive(true));
	// try {
	// //InfoTable iftlb_result=
	// exportExt.processServiceRequest("PostMultipart", vc);
	// responseheaders=contentLoader.PostMultipart(th_PlatformInfo.GetStringPropertyValue(Const.str_PlatformInfo_URLPropertyName)+
	// "/ExtensionPackageUploader?purpose=import&validate=false",
	// str_FileRepository,
	// str_FileRepoPath+FileRepoPath,
	// null, null, null,
	// headers, true,
	// null, null,
	// null, null, null, null, null, null);
	// //responseheaders = iftlb_result.toJSON();
	// } catch (Exception e) {
	//
	// StringWriter errors = new StringWriter();
	// e.printStackTrace(new PrintWriter(errors));
	// _logger.error(errors.toString());
	//
	// }
	// _logger.trace("Exiting Service: ImportExtension");
	// return responseheaders;
	// }

	@ThingworxServiceDefinition(name = "GetConfigurationTableValue", description = "", category = "Helper", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "STRING", aspects = {})
	public String GetConfigurationTableValue(
			@ThingworxServiceParameter(name = "Key", description = "", baseType = "STRING") String Key) {

		return ((String) getConfigurationSetting(Const.str_ConfTableName, Key));

	}

	@ThingworxServiceDefinition(name = "Checkout", description = "", category = "", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "NOTHING", aspects = {})
	public void Checkout(
			@ThingworxServiceParameter(name = "BranchNameOrCommit", description = "Switches the working tree to the specified branch. This is a wrapper on top of checkout <branch>.It does not autocreate new branches.", baseType = "STRING", aspects = {
					"defaultValue:master" }) String BranchNameOrCommit)
			throws Throwable, GitAPIException {
		_logger.trace("Entering Service: Checkout");
		
		//String str_Branch="";
		Git myGitFolder = GetRepository();

		try {
			myGitFolder.checkout().setName(BranchNameOrCommit).call();	
		} catch (RefNotFoundException ex) {
			_logger.warn(
					"branch not found;assuming there is no local branch tracking the remote; creating a new local tracking branch for "
							+ BranchNameOrCommit);
			myGitFolder.checkout().setCreateBranch(true).setName(BranchNameOrCommit)
					.setUpstreamMode(SetupUpstreamMode.TRACK).setStartPoint("origin/" + BranchNameOrCommit).call();
		}
		
		isDetachedHead =  GetRepository().getRepository().getFullBranch().indexOf("refs/heads")!=-1 ? false:true;
		str_CurrentBranchOrCommit = BranchNameOrCommit;
		_logger.trace("Exiting Service: Checkout");
	}

	private Git GetRepository() throws IOException, GitAPIException {
		FileRepositoryThing srcRepo = (FileRepositoryThing) EntityUtilities.findEntity(str_FileRepository,
				ThingworxRelationshipTypes.Thing);
		String str_FolderPath = srcRepo.getRootPath();
		str_FolderPath += str_FileRepoPath;
		Git myGitFolder = openOrCreate(new File(str_FolderPath));
		StoredConfig config = myGitFolder.getRepository().getConfig();
		config.setString("remote", "origin", "url", str_BitbucketRepoPath);
		config.setString("remote", "origin", "fetch", "+refs/heads/*:refs/remotes/origin/*");
		config.setString("user", null, "name", str_CommitName);
		config.setString("core", null, "autocrlf", "input");
		config.setString("user", null, "email", str_CommitEmail);
		config.save();
		return myGitFolder;
	}

	@ThingworxServiceDefinition(name = "GetCurrentBranch", description = "", category = "", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "INFOTABLE", aspects = {
			"isEntityDataShape:true", "dataShape:Git.CurrentBranchStatus.DataShape" })
	public InfoTable GetCurrentBranch() throws Exception {
		_logger.trace("Entering Service: GetCurrentBranch");
		InfoTable iftbl_CurrentBranchStatus = InfoTableInstanceFactory.createInfoTableFromDataShape("Git.CurrentBranchStatus.DataShape");
		ValueCollection vc = new ValueCollection();
		
		vc.put("BranchName", new StringPrimitive(str_CurrentBranchOrCommit));
		vc.put("DetachedHEAD",new BooleanPrimitive(isDetachedHead));
		iftbl_CurrentBranchStatus.addRow(vc);
		_logger.trace("Exiting Service: GetCurrentBranch");
		return iftbl_CurrentBranchStatus;
	}

	@ThingworxServiceDefinition(name = "GetBranchList", description = "", category = "", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "INFOTABLE", aspects = {
			"isEntityDataShape:true", "dataShape:Git.BranchList.DataShape" })
	public InfoTable GetBranchList() throws Exception {
		_logger.trace("Entering Service: GetBranchList");
		InfoTable iftbl_BranchList = InfoTableInstanceFactory.createInfoTableFromDataShape("Git.BranchList.DataShape");
		Git myGit = GetRepository();
		List<Ref> branches = myGit.branchList().setListMode(ListMode.ALL).call();
		for (Iterator<Ref> iterator = branches.iterator(); iterator.hasNext();) {
			Ref ref = (Ref) iterator.next();
			ValueCollection vc = new ValueCollection();
			
			String str_LongBranchName=ref.getName();
			String str_ShortBranchName,str_BranchType;
			str_ShortBranchName = (str_LongBranchName!="HEAD")?str_LongBranchName.replace("refs/heads/", "").replace("refs/remotes/origin/", ""):"HEAD";
			str_BranchType = (str_LongBranchName!="HEAD")?(str_LongBranchName.indexOf("refs/heads/")>=0 ? "LOCAL":"REMOTE"):"HEAD";
			vc.put("BranchName", new StringPrimitive(str_LongBranchName));
			vc.put("ShortBranchName", new StringPrimitive(str_ShortBranchName));
			vc.put("BranchType", new StringPrimitive(str_BranchType));
			iftbl_BranchList.addRow(vc);
		}
		_logger.trace("Exiting Service: GetBranchList");
		return iftbl_BranchList;
	}

	@ThingworxServiceDefinition(name = "DeleteLocalBranch", description = "This method deletes a local branch. Used in the case a remote branch was deleted/pruned and you want to remove your local copy.", category = "", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "STRING", aspects = {})
	public String DeleteLocalBranch(
			@ThingworxServiceParameter(name = "BranchName", description = "Deletes a local branch", baseType = "STRING") String BranchName)
			throws IOException, GitAPIException {
		_logger.trace("Entering Service: DeleteLocalBranch");
		try {
			Git myGitFolder = GetRepository();
			myGitFolder.branchDelete().setBranchNames("refs/heads/" + BranchName).call();
			
		} catch (Exception e) {
			StringWriter errors = new StringWriter();
			e.printStackTrace(new PrintWriter(errors));
			_logger.error(errors.toString());
			return "DeleteLocalBranch Error: " + errors.toString();
		}
		_logger.trace("Exiting Service: DeleteLocalBranch");
		return "Local Branch " + BranchName + " was deleted successfully!";
	}

	@ThingworxServiceDefinition(name = "GetCommitList", description = "Get a list of the commits for the current branch; if the current index is pointing to a commit, then it will return the commit list for the Initial branch configured in the Config section", category = "", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "INFOTABLE", aspects = {
			"isEntityDataShape:true", "dataShape:Git.CommitList.DataShape" })
	public InfoTable GetCommitList() throws Exception {
		_logger.trace("Entering Service: GetCommitList");
		Git myGitFolder = GetRepository();
		InfoTable iftbl_CommitList = InfoTableInstanceFactory.createInfoTableFromDataShape("Git.CommitList.DataShape");
		//Git myGit = GetRepository();
		ObjectId obj = myGitFolder.getRepository().resolve("refs/heads/"+str_CurrentBranchOrCommit);
		if (obj==null) obj= myGitFolder.getRepository().resolve("refs/heads/"+ ((String) getConfigurationSetting(Const.str_ConfTableName, Const.str_InitialBranch)));
		if (obj !=null)
		{
		Iterable<RevCommit> commits = myGitFolder.log().add(obj).call();
		for (Iterator<RevCommit> iterator = commits.iterator(); iterator.hasNext();) {
			RevCommit commit = (RevCommit) iterator.next();
			ValueCollection vc = new ValueCollection();
			vc.put("CommitName", new StringPrimitive(commit.getShortMessage()));
			vc.put("CommitID", new StringPrimitive(commit.getId().name()));
			iftbl_CommitList.addRow(vc);
		}
		}
		
		_logger.trace("Exiting Service: GetCommitList");
		return iftbl_CommitList;
	}

	@ThingworxServiceDefinition(name = "Status", description = "", category = "", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "INFOTABLE", aspects = {
			"isEntityDataShape:true", "dataShape:Git.Status.DataShape" })
	public InfoTable Status() throws Exception {
		_logger.trace("Entering Service: Status");
		InfoTable iftbl_Status = InfoTableInstanceFactory.createInfoTableFromDataShape("Git.Status.DataShape");
			
			
			Git myGitFolder = GetRepository();
			org.eclipse.jgit.api.Status status = myGitFolder.status().call();
			
			for (String stat:status.getModified())
			{
				ValueCollection vc = new ValueCollection();
				vc.put("File", new StringPrimitive(stat));
				vc.put("Status", new StringPrimitive("Modified"));
				iftbl_Status.addRow(vc);
			}
			for (String stat:status.getAdded())
			{
				ValueCollection vc = new ValueCollection();
				vc.put("File", new StringPrimitive(stat));
				vc.put("Status", new StringPrimitive("Added"));
				iftbl_Status.addRow(vc);
			}
			for (String stat:status.getChanged())
			{
				ValueCollection vc = new ValueCollection();
				vc.put("File", new StringPrimitive(stat));
				vc.put("Status", new StringPrimitive("Changed"));
				iftbl_Status.addRow(vc);
			}
			for (String stat:status.getIgnoredNotInIndex())
			{
				ValueCollection vc = new ValueCollection();
				vc.put("File", new StringPrimitive(stat));
				vc.put("Status", new StringPrimitive("Ignored"));
				iftbl_Status.addRow(vc);
			}
			for (String stat:status.getMissing())
			{
				ValueCollection vc = new ValueCollection();
				vc.put("File", new StringPrimitive(stat));
				vc.put("Status", new StringPrimitive("Missing"));
				iftbl_Status.addRow(vc);
			}
			for (String stat:status.getRemoved())
			{
				ValueCollection vc = new ValueCollection();
				vc.put("File", new StringPrimitive(stat));
				vc.put("Status", new StringPrimitive("Removed"));
				iftbl_Status.addRow(vc);
			}
//			for (String stat:status.getUncommittedChanges())
//			{
//				ValueCollection vc = new ValueCollection();
//				vc.put("File", new StringPrimitive(stat));
//				vc.put("Status", new StringPrimitive("UNC"));
//				iftbl_Status.addRow(vc);
//			}
			for (String stat:status.getUntracked())
			{
				ValueCollection vc = new ValueCollection();
				vc.put("File", new StringPrimitive(stat));
				vc.put("Status", new StringPrimitive("Untracked"));
				iftbl_Status.addRow(vc);
			}
			for (String stat:status.getUntrackedFolders())
			{
				ValueCollection vc = new ValueCollection();
				vc.put("File", new StringPrimitive(stat));
				vc.put("Status", new StringPrimitive("UntrackedFolder"));
				iftbl_Status.addRow(vc);
			}
			
			myGitFolder.getRepository().close();
			myGitFolder.close();
		_logger.trace("Exiting Service: Status");
		return iftbl_Status;
	}

	@ThingworxServiceDefinition(name = "GetDiffPerFile", description = "", category = "", isAllowOverride = false, aspects = {
			"isAsync:false" })
	@ThingworxServiceResult(name = "Result", description = "", baseType = "STRING", aspects = {})
	public String GetDiffPerFile(
			@ThingworxServiceParameter(name = "File", description = "", baseType = "STRING") String File) throws Exception, GitAPIException {
		_logger.trace("Entering Service: GetDiffPerFile");
		if (File!=null){
		Git myGitFolder = GetRepository();
		ByteArrayOutputStream dif = new ByteArrayOutputStream();
		myGitFolder.diff().setPathFilter(PathFilter.create(File)).setOutputStream(dif).call();
		
		
		_logger.trace("Exiting Service: GetDiffPerFile");
		return dif.toString("UTF-8");}
		else return "";
	}



	// @ThingworxServiceDefinition(name = "ExportSourceControEntities",
	// description = "", category = "", isAllowOverride = false, aspects = {
	// "isAsync:false" })
	// @ThingworxServiceResult(name = "Result", description = "", baseType =
	// "NOTHING", aspects = {})
	//
	// public void ExportSourceControEntities(@ThingworxServiceParameter(name =
	// "repositoryName", description = "File repository to which to export the
	// content", baseType = "THINGNAME", aspects = {
	// "thingTemplate:'FileRepository, SourceControlRepository'" }) final String
	// repositoryName, @ThingworxServiceParameter(name = "path", description =
	// "Path to which to write the content", baseType = "STRING") String path,
	// @ThingworxServiceParameter(name = "collection", description = "Collection
	// name you wish to export (optional)", baseType = "STRING") final String
	// collection, @ThingworxServiceParameter(name = "tags", description = "Tags
	// (optional)", baseType = "TAGS", aspects = { "tagType:ModelTags" }) final
	// TagCollection tags, @ThingworxServiceParameter(name = "startDate",
	// description = "Date and time from which to export entities by their last
	// modified date (optional)", baseType = "DATETIME") final DateTime
	// startDate, @ThingworxServiceParameter(name = "endDate", description =
	// "Date and time to which to export entities by their last modified date
	// (optional)", baseType = "DATETIME") final DateTime endDate,
	// @ThingworxServiceParameter(name = "projectName", description = "Project
	// name (optional)", baseType = "PROJECTNAME") final String projectName,
	// @ThingworxServiceParameter(name = "includeDependents", description =
	// "Include dependent projects (optional)", baseType = "BOOLEAN", aspects =
	// { "defaultValue:false" }) final Boolean includeDependents) throws
	// Exception {
	// _logger.trace("Entering Service: ExportSourceControEntities");
	// Resource rsc = (Resource)
	// EntityUtilities.findEntity("SourceControlFunctions",
	// ThingworxRelationshipTypes.Resource);
	// ValueCollection vc = new ValueCollection();
	// vc.put("repositoryName", new StringPrimitive(repositoryName));
	// vc.put("path", new StringPrimitive(path));
	// vc.put("collection", new StringPrimitive(collection));
	// vc.put("tags", new TagCollectionPrimitive(tags));
	// vc.put("startDate", new DatetimePrimitive(startDate));
	// vc.put("endDate", new DatetimePrimitive(endDate));
	// vc.put("projectName", new StringPrimitive(projectName));
	//
	// vc.put("includeDependents", new BooleanPrimitive(includeDependents));
	//
	// rsc.processServiceRequest("ExportSourceControlledEntities", vc);
	//
	//
	//
	//
	//
	// _logger.trace("Exiting Service: ExportSourceControEntities");
	// }
	//

}
