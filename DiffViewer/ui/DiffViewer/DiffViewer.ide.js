TW.IDE.Widgets.DiffViewer = function () {
	this.widgetProperties = function () {
		return {
			'name' : 'DiffViewer',
			'description' : 'Pretty print git diffs.',
			'category' : ['Common'],
            'isResizable': true,
			'supportsAutoResize': true,
			'supportsLabel': true,
			'isExtension':true,
			'properties' : {
					
				'InputText': {
                    'baseType' : 'STRING',
					'isVisible': true,
					'defaultValue' : '',
					 'isBindingTarget': true
                }
			}
		};
	};
	 this.widgetServices = function () {
    };
	 this.renderHtml = function () {
	
        var html = '';
        html += '<div class="widget-content widget-DiffViewer">';
        html += '</div>';
        return html;
    };
	
	this.afterRender = function () {
};
	
	    this.afterSetProperty = function (name, value) {
		return false;
    };

};