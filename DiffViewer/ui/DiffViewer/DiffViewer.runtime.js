TW.Runtime.Widgets.DiffViewer = function () {
	var valueElem;
	var htmlToWrite = '';
	var widgetElement;
	var thisWidget = this;
	
	var args={
        source: null,
        mode  : "beautify",
        lang  : "auto"
    };
	  this.runtimeProperties = function () {
	        return {
                'supportsAutoResize': true
	        };
	    };
		
	this.renderHtml = function () {
		args.source=this.GetText();
		var html='<div class="widget-content widget-DiffViewer"></div>';
		return html;
	};
	
	this.GetText = function()
	{
		return this.getProperty("InputText");
	}
	
	this.afterRender = function () {
	};

	
		
		
	this.updateProperty = function (updatePropertyInfo) {
		
		
		// TargetProperty tells you which of your bound properties changed
		if (updatePropertyInfo.TargetProperty === 'InputText') {
			//var html=global.Diff2Html.getPrettyHtmlFromDiff(updatePropertyInfo.SinglePropertyValue,{inputFormat:'diff',outputFormat: 'line-by-line'});
			var html=Diff2Html.getPrettyHtmlFromDiff(updatePropertyInfo.SinglePropertyValue,{inputFormat:'diff',outputFormat: 'line-by-line'});
			this.jqElement.html(html);
			this.setProperty('Text', updatePropertyInfo.SinglePropertyValue);
		}
	};
};